#include <iostream>
using namespace std;


//立方体类设计
//1、创建立方体类
//2、设计属性
//3、设计行为 获取立方体的面积和体积
//4、分别利用全局函数和成员函数 判断两个立方体是否相等

class Cube
{
private:
	//属性
	int m_L;  //长
	int m_W;  //宽
	int m_H;  //高
public:
	//行为
	//设置长宽高
	void setL(int l)
	{
		m_L = l;
	 }

	void setW(int w)
	{
		m_W = w;
	}

	void setH(int h)
	{
		m_H = h;
	}

	//设置获取长宽高 
	int getL()
	{
		return m_L;
	}
	 
	int getW()
	{
		return m_W;
	}

	int getH()
	{
		return m_H;
	}

	//获取立方体面积
	int calculateS()
	{
		return 2 * m_L * m_W + 2 * m_W * m_H + 2 * m_H * m_L;
	}

	//获取立方体体积
	int calculateV()
	{
		return m_L * m_W * m_H;
	}

	//利用成员函数判断两个立方体是否相等
	bool isSameByClass(Cube &c)
	{
		if (m_L == c.getL() && m_W == c.getW() && m_H == c.getH())
			return true;
		else
			return false;

	}

};

//利用全局函数判断 两个立方体是否相等
bool isSame(Cube& c1, Cube& c2)
{
	if (c1.getL() == c2.getL() && c1.getW() == c2.getW() && c1.getH() == c2.getH())
		return true;
	else
		return false;
}
void main_Cube(void)
{
	  //创建立方体对象
	Cube c1;
	c1.setL(10);
	c1.setW(10);
	c1.setH(10);
	
	Cube c2;
	c2.setL(10);
	c2.setW(10);
	c2.setH(10);

	bool ret = c1.isSameByClass(c2);
	if (ret)
		cout << "c1和c2是相等的" << endl;
	else
		cout << "c1和c2不相等" << endl;

	/*cout << "c1的面积为：" << c1.calculateS() << endl;
	cout << "c1的体积为：" << c1.calculateV() << endl;*/

	system("pause");
}